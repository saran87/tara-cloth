<!DOCTYPE html>
<html lang="en">
<?php $title="Home"; ?>
<?php require 'head.php' ?>

<body class="bordered">

    <!-- Preloader Two -->
    <div id="preloader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
    <div id="wrapper" class="main-wrapper">
    <?php require 'navigation.php' ?>
        <!--
            Home Banner====================================
        -->
        <div id="start" class="parallax-home parallax one fullscreen">
            <div class="container">
                <div class="distplay-table fullscreen">
                </div>
            </div>
        </div>
        <!--
            End Home
            Banner====================================
        -->

        <div class="sc_cta">
            <div class="container">
                <p>Get one step closer to launching your own clothing line by Registering Now
                </p>
                <a href="./registration.php">Register Now</a>
            </div>
        </div>

        <section id="service" class="">
            <div class="container">
                <div class="row">
                    <header class="col-lg-12 section-head text-center item_top">
                        <h2>We help create a business model that suits your financial needs and offer strategic assistance, from registering your company to launching your own clothing line within a few months</h2>
                    </header>

                    <div class="col-md-10 col-md-offset-1">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4
                                                    col-md-4 text-center
                                                    item_left">
                                <div class="service-item">
                                    <h4>
                                        <a href="./index.php">Logo & Website Design</a>
                                    </h4>
                                    <img style="padding-top:20px;" src="./img/agency/design.png">
                                    <p>We offer different styles of website creation to suit your requirements. From graphic designs, logo creation to completing your website the way you want it.</p>
                                </div>
                            </div>
                            <!-- end .service-item
                                                    -->

                            <div class="col-xs-12 col-sm-4
                                                    col-md-4 text-center
                                                    item_bottom">
                                <div class="service-item">
                                    <h4>
                                        <a href="./index.php">You Design & We Manufacture</a>
                                    </h4>
                                    <img style="padding-top:20px;" src="./img/agency/production.png">
                                    <p>As fashion influencers you tell us what the latest trends are, show us pictures, send us samples, and we will have our manufacturing team create your vision into a product you can sell.
                                    </p>
                                </div>
                            </div>
                            <!-- end .service-item -->

                            <div class="col-xs-12 col-sm-4
                                                    col-md-4 text-center
                                                    item_right">
                                <div class="service-item">
                                    <h4>
                                        <a href="./index.php">Package Design & Manufacturing</a>
                                    </h4>
                                    <img style="padding-top:20px;" src="./img/agency/shipping.png">
                                    <p>Show us what style of boxes you like by sending us pictures and samples. We will have our manufacturing team create the perfect box to suit the needs of your apparel.
                                    </p>
                                </div>
                            </div>
                            <!-- end .service-item -->
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>

    <section id="latest-course" class="killPaddingBottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-10
                                        col-md-offset-1 col-lg-8
                                        col-lg-offset-2">
                    <div class="section-head text-center">
                        <h2>Order A Free Sample</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="course-item">
                        <div class="course-thumb">
                            <a href="./registration.php">
                                <img src="./img/icons/product_1.png" alt="" />
                            </a>
                        </div>
                        <div class="course-entry">
                            <div class="separator">
                                <a href="./registration.php">
                                    <i class="fa fa-bars"></i>
                                    Order Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="course-item">
                        <div class="course-thumb">
                            <a href="./registration.php">
                                <img src="./img/icons/product_2.png" alt="">
                            </a>
                        </div>
                        <div class="course-entry">
                            <div class="separator">
                                <a href="./registration.php">
                                    <i class="fa fa-bars"></i>
                                    Order Now</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="course-item">
                        <div class="course-thumb">
                            <a href="./registration.php">
                                <img src="./img/icons/product_3.png" alt="">
                            </a>
                        </div>
                        <div class="course-entry">
                            <div class="separator">
                                <a href="./registration.php">
                                    <i class="fa fa-bars"></i>
                                    Order Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <br>

    <section style="padding-top:60px;" class="parallax23
                            parallax">
        <div class="overlay"></div>
        <div class="container">
            <div class="sc_testimonial white friends-say
                                    killPadding">
                <div class="flexslider sc_testimonial_1
                                        control-one">
                    <ul class="slides">
                        <li>
                            <div class="sc_single_testimonial">
                                <img src="./img/agency/woman1.png" alt="">
                                <p>After I Joined with Tara, my Business has Increaed 200% in Woman's Leggings</p>
                                <span>&#8212; -Dot Fashions, NY
                                </span>
                            </div>
                        </li>
                        <li>
                            <div class="sc_single_testimonial">
                                <img src="./img/agency/man.png" alt="">
                                <p>Tara has Changed my Life...Never though I could make easy money from Home by Selling e-Commerce.
                                    Thanks Tara.</p>
                                <span>&#8212; -Peter, LA</span>
                            </div>
                        </li>
                        <li>
                            <div class="sc_single_testimonial">
                                <img src="./img/agency/woman1.png" alt="">
                                <p>Tara has Unique Products. Just like e-commerce, their Products has a Great potential in On-Store
                                    Purchase too.</p>
                                <span>&#8212; -Mary, Seattle</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Team==================================== -->

    <!-- End Team==================================== -->
    <section id="client" class="our--client">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <header class="section-head style_3
                                            text-left">
                        <h2>#taracloth</h2>
                        <p>Get Socially Connected with Us, and Exponentially Multiply Your Customer Base, around the World.
                            Register with Us now to Know more
                        </p>
                    </header>
                </div>
                <div class="col-md-6">
                    <div class="client--list">
                        <ul class="text-center">
                            <li>
                                <a href="https://www.facebook.com/TaraClothing90-174693836454989/">
                                    <img src="./img/agency/facebook.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/taraclothing90/">
                                    <img src="./img/agency/insta.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.pinterest.com/taraclothing90/pins/">
                                    <img src="./img/agency/pinterest.png" alt="">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/taraclothing/">
                                    <img src="./img/agency/linkedin.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
                            footer widget====================================
                            -->

    <!--
                            End footer
                            widget====================================
                            -->

    <!--
                            footer==================================== -->
    <footer class="footer">
        <div class="container">
            <p class="copyright pull-left item_left">Copyright &copy; 2018. All rights reserved @ Tara Clothing
                </a>.</strong>
            </p>
           <ul class="pull-right social-links text-center item_right">
                    <li>
                        <a href="https://www.instagram.com/taraclothing90/">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
            </ul>
        </div>
    </footer>
    <!--
                    End footer==================================== -->

    <!-- back to top -->
    <a href="" id="go-top">
        <i class="fa fa-angle-up"></i>
        top
    </a>
    <!-- end #go-top -->
    </div>

    <!--
                JavaScripts========================== -->
    <!-- main jQuery -->
    <script type="text/javascript" src="./js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="./js/vendor/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="./js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="./js/jquery.appear.js"></script>
    <script type="text/javascript" src="./js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="./js/vendor/owl.carousel.min.js"></script>
    <script type="text/javascript" src="./js/jquery.backstretch.min.js"></script>
    <script type="text/javascript" src="./js/jquery.nav.js"></script>
    <script type="text/javascript" src="./js/lightbox.min.js"></script>
    <script type="text/javascript" src="./js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="./js/jquery-validation.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./js/form.min.js"></script>
    <script type="text/javascript" src="./js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="./js/jquery.multiscroll.min.js"></script>
    <script type="text/javascript" src="./js/jquery-countTo.js"></script>
    <script type="text/javascript" src="./js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="./js/jquery.selectbox-0.2.min.js"></script>
    <script type="text/javascript" src="./js/tweetie.min.js"></script>
    <script type="text/javascript" src="./js/jquery.sticky.js"></script>
    <script type="text/javascript" src="./js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="./js/jquery.circliful.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easypiechart.js"></script>
    <script type="text/javascript" src="./js/masonry.pkgd.js"></script>
    <script type="text/javascript" src="./js/kenburned.min.js"></script>
    <script type="text/javascript" src="./js/mediaelement-and-player.min.js"></script>
    <!-- shortcode scripts -->
    <script type="text/javascript" src="./shortcodes/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.slicebox.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.eislideshow.js"></script>
    <script type="text/javascript" src="./shortcodes/js/camera.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.zaccordion.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/main.js"></script>
    <script type="text/javascript" src="./js/jquery.prettySocial.min.js"></script>
    <script type="text/javascript" src="./js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="./js/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="./js/jquery.countdown.js"></script>
    <script type="text/javascript" src="./js/jquery.webticker.min.js"></script>
    <script type="text/javascript" src="./js/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="./js/selectize.min.js"></script>
    <!-- image filter -->
    <script type="text/javascript" src="./js/img-filter/jquery.gray.min.js"></script>
    <!-- // image filter -->
    <script type="text/javascript" src="./js/wow.min.js"></script>
    <script src="./syntax-highlighter/scripts/prettify.min.js"></script>
    <script type="text/javascript">
        $.SyntaxHighlighter.init();
    </script>
    <script type="text/javascript" src="./js/main.js"></script>
    <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8RV1M5dAKCJUP7USf_DSABjfCeNLX_ko&callback=googleMap"></script>
</body>

</html>
