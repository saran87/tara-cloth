<!DOCTYPE html>
<html lang="en">

<?php $title="Contact Us"; ?>
<?php require 'head.php' ?>

<body class="bordered">

    <!-- Preloader Two -->
    <div id="preloader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <div id="wrapper" class="main-wrapper">
        <?php require 'navigation.php' ?>

        <!--
        Contact
        ==================================== -->
        <section id="contact" class="contact">
            <div class="container">
                <div style="padding-top:50px; padding-bottom:50px;" class="row contact-page">
                    <header class="row section-head text-center item_top">
                        <h2>Contact Us</h2>
                    </header>
                    <div class="col-md-8">
                       <div class="img-container">
                                <img src="images/daniel-chen-108577-unsplash.jpg" class="img-rounded" alt="Snow" style="width:100%;">
                                <!-- <div class="bottom-left">Bottom Left</div>
                                <div class="top-left">Top Left</div>
                                <div class="top-right">Top Right</div>
                                <div class="bottom-right">Bottom Right</div> -->
                                <div class="centered"> <a role="button" class="btn btn-lg btn-default" href="https://www.instagram.com/taraclothing90/">
                                   Let’s Meetup
                                    </a></div>
                                </div>

                    </div>
                    <div class="col-md-3 col-md-offset-1 item_top justify-content-end">
                        <div class="address">
                            <p class="text-left">
                                <strong>Tara Clothing Head Office</strong>
                            </p>
                            <p class="text-left">
                                <i class="fa fa-map-marker"></i>Royal Oak, Michigan 48073.</p>
                            <p class="text-left">
                                <i class="fa fa-phone"></i>Phone: +1 (585) 880-3124</p>
                            <p class="text-left">
                                <i class="fa fa-envelope"></i>
                                <a href="">contact@taraclothing.com</a>
                            </p>
                            <p class="text-left">
                                <i class="fa fa-link"></i>
                                <a target="_blank" href="http://taracloth.com/">http://taracloth.com/</a>
                            </p>
                        </div>
                    </div>
                </div>

            </div>

    </div>
    </section>
    <!--
        End Contact
        ==================================== -->

    <!--
        footer
        ==================================== -->
    <footer class="footer">
        <div class="container">
            <p class="copyright pull-left item_left">Copyright &copy; 2018. All rights reserved @ Tara Clothing</a>.</strong>
            </p>
            <ul class="pull-right social-links text-center item_right">
                    <li>
                        <a href="https://www.instagram.com/taraclothing90/">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
            </ul>
        </div>
    </footer>
    <!--
        End footer
        ==================================== -->

    <!-- back to top -->
    <a href="" id="go-top">
        <i class="fa fa-angle-up"></i>
        top
    </a>
    <!-- end #go-top -->
    </div>
    <!--
        JavaScripts
        ========================== -->
    <!-- main jQuery -->
    <script type="text/javascript" src="./js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="./js/vendor/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="./js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="./js/jquery.appear.js"></script>
    <script type="text/javascript" src="./js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="./js/vendor/owl.carousel.min.js"></script>
    <script type="text/javascript" src="./js/jquery.backstretch.min.js"></script>
    <script type="text/javascript" src="./js/jquery.nav.js"></script>
    <script type="text/javascript" src="./js/lightbox.min.js"></script>
    <script type="text/javascript" src="./js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="./js/jquery-validation.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./js/form.min.js"></script>
    <script type="text/javascript" src="./js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="./js/jquery.multiscroll.min.js"></script>
    <script type="text/javascript" src="./js/jquery-countTo.js"></script>
    <script type="text/javascript" src="./js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="./js/jquery.selectbox-0.2.min.js"></script>
    <script type="text/javascript" src="./js/tweetie.min.js"></script>
    <script type="text/javascript" src="./js/jquery.sticky.js"></script>
    <script type="text/javascript" src="./js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="./js/jquery.circliful.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easypiechart.js"></script>
    <script type="text/javascript" src="./js/masonry.pkgd.js"></script>
    <script type="text/javascript" src="./js/kenburned.min.js"></script>
    <script type="text/javascript" src="./js/mediaelement-and-player.min.js"></script>
    <!-- shortcode scripts -->
    <script type="text/javascript" src="./shortcodes/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.slicebox.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.eislideshow.js"></script>
    <script type="text/javascript" src="./shortcodes/js/camera.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.zaccordion.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/main.js"></script>
    <script type="text/javascript" src="./js/jquery.prettySocial.min.js"></script>
    <script type="text/javascript" src="./js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="./js/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="./js/jquery.countdown.js"></script>
    <script type="text/javascript" src="./js/jquery.webticker.min.js"></script>
    <script type="text/javascript" src="./js/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="./js/selectize.min.js"></script>
    <!-- image filter -->
    <script type="text/javascript" src="./js/img-filter/jquery.gray.min.js"></script>
    <!-- // image filter -->
    <script type="text/javascript" src="./js/wow.min.js"></script>
    <script src="./syntax-highlighter/scripts/prettify.min.js"></script>
    <script type="text/javascript">
        $.SyntaxHighlighter.init();
    </script>
    <script type="text/javascript" src="./js/main.js"></script>
    <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8RV1M5dAKCJUP7USf_DSABjfCeNLX_ko&callback=googleMap"></script>
</body>

</html>
