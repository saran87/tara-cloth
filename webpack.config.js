const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const indexHtml = path.join(__dirname, ".", "index.php");

module.exports = {
    mode: 'development',
    entry: [indexHtml],
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            template: './index.php',
            title: 'Tara Clothing'
        })
    ],
    module: {
        rules: [
            { test: /\.(png|svg|jpg|gif)$/, use: ["file-loader"]},
            {
                test: /\.css$/,
                use: [
                    'css-loader',
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.html$/,
                exclude: /(javascript.|mailto\:contact\@taraclothing.)/,
                use: [{
                    loader: 'html-loader',
                }],
            }
        ]
    },
    externals: {
        'javascript:;': 'JQuery',
        window:'window'
    }
};
