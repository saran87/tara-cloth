<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Multiple recipients
    $to = 'contact@taracloth.com'; // note the comma

    // Subject
    $subject = 'New Registration @ TaraCloth';

    // Message
    $message = <<<EOT
    <html>
    <head>
    <title>New enquiry</title>
    </head>
    <body>
    <p>Below are the details of the enquiry!</p>
    <table border="0" cellspacing="5" cellpadding="5" style="background-color:#CCCCCC; text-align:center;">
        <tr>
        <th></th><th></th>
        </tr>
        <tr>
        <td>First Name</td><td>{$_REQUEST["fname"]}</td>
        </tr>
        <tr>
        <td>Last Name</td><td>{$_REQUEST["lname"]}</td
        </tr>
        <tr>
        <td>Phone</td><td>{$_REQUEST["phone"]}</td
        </tr>
        <tr>
        <td>Email</td><td>{$_REQUEST["email"]}</td
        </tr>
        <tr>
        <td>Phone</td><td>{$_REQUEST["phone"]}</td
        </tr>
        <tr>
        <td>Instagram</td><td>{$_REQUEST["insta"]}</td
        </tr>
        <tr>
        <td>Video</td><td>{$_REQUEST["video"]}</td
        </tr>
        <tr>
        <td>Company</td><td>{$_REQUEST["company"]}</td
        </tr>
        <tr>
        <td>Site</td><td>{$_REQUEST["site"]}</td
        </tr>
    </table>
    </body>
    </html>
EOT;
    // To send HTML mail, the Content-type header must be set
    $headers[] = 'MIME-Version: 1.0';
    $headers[] = 'Content-type: text/html; charset=iso-8859-1';

    // Additional headers
    $headers[] = 'To: Tara Cloth <contact@taracloth.com>';
    $headers[] = 'From: Tara Cloth <contact@taracloth.com>';

    // Mail it
    mail($to, $subject, $message, implode("\r\n", $headers));
    $email_sent = true;
}
?>
<!DOCTYPE html>
<html lang="en">

<?php $title="Register"; ?>
<?php require 'head.php' ?>

<body class="bordered">

    <!-- Preloader Two -->
    <div id="preloader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <div id="wrapper" class="main-wrapper">
    <?php require 'navigation.php' ?>

        <section class="contact2">
            <div class="bg-colorleft gray"></div>
            <div class="bg-color left white"></div>
            <div class="container">
                <div style="padding-top:50px; padding-bottom:50px;" class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 item_right">
                        <?php
                            if (isset($email_sent) && $email_sent){
                                    echo "<div class='alert alert-success' role='alert'> <strong>We notified our team!</strong> Someone will keep in touch with you soon to help you get started</div>";
                            }
                        ?>
                        <h3 class="subtitle">Register and get started</h3>
                        <div class="row">
                            <form action="#" method="post" id="contact-form" class="contact-form">
                                <div class="col-md-6">
                                    <input type="text" name="fname" placeholder="First Name" required class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="lname" placeholder="Last Name" required class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" name="email" placeholder="Email" required class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="phone" name="phone" placeholder="Phone" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="insta" name="insta" required placeholder="Provide us your insta link" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="followers" name="video" placeholder="Provide your youtube channel link" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="company" name="company" placeholder="Company Name" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="site" name="site" placeholder="Provide your Website/Blog link" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <input type="promo" name="promo" placeholder="Promo Code" class="form-control">
                                </div>

                                <div class="col-md-12">
                                    <textarea name="message" class="form-control" placeholder="Tell us what kind of apparel do you wish to launch"></textarea>
                                    <input type="submit" value="Register Now" class="message-sub pull-right btn btn-blue">
                                </div>
                            </form>
                        </div>
                        <div id="success">
                            <p>Your message was sent succssfully! I will be in touch as soon as I can.</p>
                        </div>
                        <div id="error">
                            <p>Something went wrong, try refreshing and submitting the form again.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--
        footer
        ==================================== -->
        <footer class="footer">
            <div class="container">
                <p class="copyright pull-left item_left">Copyright &copy; 2018. All rights reserved @ Tara Clothing</a>.</strong>
                </p>
                <ul class="pull-right social-links text-center item_right">
                    <li>
                        <a href="https://www.instagram.com/taraclothing90/">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
        <!--
        End footer
        ==================================== -->

        <!-- back to top -->
        <a href="" id="go-top">
            <i class="fa fa-angle-up"></i>
            top
        </a>
        <!-- end #go-top -->
    </div>
    <!--
        JavaScripts
        ========================== -->
    <!-- main jQuery -->
    <script type="text/javascript" src="./js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="./js/vendor/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="./js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="./js/jquery.appear.js"></script>
    <script type="text/javascript" src="./js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="./js/vendor/owl.carousel.min.js"></script>
    <script type="text/javascript" src="./js/jquery.backstretch.min.js"></script>
    <script type="text/javascript" src="./js/jquery.nav.js"></script>
    <script type="text/javascript" src="./js/lightbox.min.js"></script>
    <script type="text/javascript" src="./js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="./js/jquery-validation.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./js/form.min.js"></script>
    <script type="text/javascript" src="./js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="./js/jquery.multiscroll.min.js"></script>
    <script type="text/javascript" src="./js/jquery-countTo.js"></script>
    <script type="text/javascript" src="./js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="./js/jquery.selectbox-0.2.min.js"></script>
    <script type="text/javascript" src="./js/tweetie.min.js"></script>
    <script type="text/javascript" src="./js/jquery.sticky.js"></script>
    <script type="text/javascript" src="./js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="./js/jquery.circliful.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easypiechart.js"></script>
    <script type="text/javascript" src="./js/masonry.pkgd.js"></script>
    <script type="text/javascript" src="./js/kenburned.min.js"></script>
    <script type="text/javascript" src="./js/mediaelement-and-player.min.js"></script>
    <!-- shortcode scripts -->
    <script type="text/javascript" src="./shortcodes/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.slicebox.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.eislideshow.js"></script>
    <script type="text/javascript" src="./shortcodes/js/camera.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.zaccordion.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/main.js"></script>
    <script type="text/javascript" src="./js/jquery.prettySocial.min.js"></script>
    <script type="text/javascript" src="./js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="./js/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="./js/jquery.countdown.js"></script>
    <script type="text/javascript" src="./js/jquery.webticker.min.js"></script>
    <script type="text/javascript" src="./js/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="./js/selectize.min.js"></script>
    <!-- image filter -->
    <script type="text/javascript" src="./js/img-filter/jquery.gray.min.js"></script>
    <!-- // image filter -->
    <script type="text/javascript" src="./js/wow.min.js"></script>
    <script src="./syntax-highlighter/scripts/prettify.min.js"></script>
    <script type="text/javascript">
        $.SyntaxHighlighter.init();
    </script>
    <script type="text/javascript" src="./js/main.js"></script>
    <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8RV1M5dAKCJUP7USf_DSABjfCeNLX_ko&callback=googleMap"></script>
</body>

</html>
