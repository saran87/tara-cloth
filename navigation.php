   <!--
        Navigation
        ==================================== -->
        <header id="head" class="navbar-default sticky-header">
            <div class="container">
                <div class="mega-menu-wrapper border clearfix">
                    <div class="navbar-header">
                        <!-- responsive nav button -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- /responsive nav button -->
                        <!-- logo -->
                        <a class="navbar-brand" href="./index.php">
                            <img src="./img/icons/logo.png" class="thumbnail" alt="TaraClothing">
                        </a>
                        <!-- /logo -->
                    </div>
                    <!-- main nav -->
                    <nav class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li class="<?php echo $title === 'Home' ? 'current' : ''; ?>">
                                <a href="./index.php">Home</a>
                            </li>
                            <li class="<?php echo $title === 'How it Works' ? 'current' : ''; ?>">
                                <a href="./how_it_works.php">How it Works ?</a>
                            </li>
                            <li class="<?php echo $title === 'Register' ? 'current' : ''; ?>">
                                <a href="./registration.php">Begin Registration</a>
                            </li>
                            <li class="<?php echo $title === 'Contact Us' ? 'current' : ''; ?>">
                                <a href="./contact_us.php">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /main nav -->
                </div>
            </div>
        </header>
        <!--
        End Navigation
        ==================================== -->
