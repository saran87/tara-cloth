<!DOCTYPE html>
<html lang="en">
<?php $title="How it Works"; ?>
<?php require 'head.php' ?>

<body class="bordered">

    <!-- Preloader Two -->
    <div id="preloader">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>

    <div id="wrapper" class="main-wrapper">
     <?php require 'navigation.php' ?>
        <div style="padding-top:40px;">
            <center>
                <h1> How it Works</h1>
            </center>
        </div>
        <div class="pages faq-page">
            <div class="container">
                <div class="faq-wrapper panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="faq-item panel">
                        <div role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                We set you up with everything you need to successfully launch your own clothing line
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="faq-body panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                            <img src="./img/agency/hiw_team.jpg">
                                    </div>
                                    <div class="col-md-8 col-md-offset-1">
                                        <ul class="list-group">
                                            <li class="list-group-item">Business Model Creation</li>
                                            <li class="list-group-item">Logo and Website Creation</li>
                                            <li class="list-group-item">Design & Manufacturing of your Apparel</li>
                                            <li class="list-group-item">Package and Shipping of your completed product</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end .faq-item -->
                    <div class="faq-item panel">
                        <div role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                 Exclusive Business Team
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="faq-body panel-collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="row">
                                <div class="col-md-3">
                                    <p>
                                        <img style="float:left; padding-right:30px; padding-bottom:20px;" src="./img/agency/hiw_fd.jpg">
                                    </p>
                                </div>
                                <div class="col-md-8 col-md-offset-1">
                                    <p>We give you an exclusive team of website and graphic designers along with an account manager that will help coordinate the design and manufacture of your clothing line, box creation for your apparel and finally ship them to you.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end .faq-item -->

                    <div class="faq-item panel">
                        <div role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                Our Design Team
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="faq-body panel-collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="row">
                                <div class="col-md-3">
                                    <p>
                                        <img style="float:left; padding-right:30px; padding-bottom:20px;" src="./img/agency/hiw_fd1.jpg">
                                    </p>
                                </div>
                                <div class="col-md-8 col-md-offset-1">
                                    <p>Work with our in-house designers to create your own website, apparel and creative boxes for your apparel. We provide you with everything you need to create and launch your own clothing line.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end .faq-item -->

                    <div class="faq-item panel">
                        <div role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                 Manufacture Anything
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="faq-body panel-collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="row">
                                <div class="col-md-3">
                                <p>
                                    <img src="./img/agency/hiw_production.jpg">
                                </p>
                                </div>
                                <div class="col-md-8 col-md-offset-1">
                                <p>When it comes to apparel and boxes, we pride ourselves on being able to manufacture almost anything you want. Leggings, Activewear, Swimsuits, Jeans, T-shirts, dresses, etc... you name it, and we have it covered. Register now to get in touch with us and let’s get started.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end .faq-item -->
                    <div class="faq-item panel">
                        <div role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                 A Fashion Influencers Dream
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="faq-body panel-collapse" role="tabpanel" aria-labelledby="headingSix">
                            <div class="row">
                                <div class="col-md-3">
                                    <p>
                                        <img style="float:left; padding-right:30px; padding-bottom:20px;" src="./img/agency/hiw_lead.jpg">
                                    </p>
                                </div>
                                <div class="col-md-8 col-md-offset-1">
                                    <p>We understand that launching a clothing line can be a big deal and we also understand that it is not uncommon to have a fulltime job and still be a successful influencer.  If you’re a fashion influencer on Instagram or other social media platforms and the thought of launching your own clothing line has crossed your mind, then you are on the right path to success. Allow us to help you launch your business. Lets get started by <a class="btn btn-default" href="./registration.php">Registering Now</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end .faq-item -->
                </div>
                <!-- end .faq-wrapper -->
            </div>
            <!-- end .container -->
        </div>
        <!-- end .portdolio-item-single -->

        <div class="sc_cta">
            <div class="container">
                <p>Start Your Registration Now</p>
                <a href="./registration.php">Begin Registration</a>
            </div>
        </div>
        <!--
        footer
        ==================================== -->
        <footer class="footer">
            <div class="container">
                <p class="copyright pull-left item_left">Copyright &copy; 2018. All rights reserved @ Tara Clothing</a>.</strong>
                </p>
                <ul class="pull-right social-links text-center item_right">
                    <li>
                        <a href="https://www.instagram.com/taraclothing90/">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
        <!--
        End footer
        ==================================== -->

        <!-- back to top -->
        <a href="" id="go-top">
            <i class="fa fa-angle-up"></i>
            top
        </a>
        <!-- end #go-top -->
    </div>
    <!--
        JavaScripts
        ========================== -->
    <!-- main jQuery -->
    <script type="text/javascript" src="./js/vendor/modernizr-2.6.2.min.js"></script>
    <script type="text/javascript" src="./js/vendor/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="./js/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="./js/jquery.appear.js"></script>
    <script type="text/javascript" src="./js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="./js/vendor/owl.carousel.min.js"></script>
    <script type="text/javascript" src="./js/jquery.backstretch.min.js"></script>
    <script type="text/javascript" src="./js/jquery.nav.js"></script>
    <script type="text/javascript" src="./js/lightbox.min.js"></script>
    <script type="text/javascript" src="./js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="./js/jquery-validation.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="./js/form.min.js"></script>
    <script type="text/javascript" src="./js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="./js/jquery.multiscroll.min.js"></script>
    <script type="text/javascript" src="./js/jquery-countTo.js"></script>
    <script type="text/javascript" src="./js/jflickrfeed.min.js"></script>
    <script type="text/javascript" src="./js/jquery.selectbox-0.2.min.js"></script>
    <script type="text/javascript" src="./js/tweetie.min.js"></script>
    <script type="text/javascript" src="./js/jquery.sticky.js"></script>
    <script type="text/javascript" src="./js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" src="./js/jquery.circliful.min.js"></script>
    <script type="text/javascript" src="./js/jquery.easypiechart.js"></script>
    <script type="text/javascript" src="./js/masonry.pkgd.js"></script>
    <script type="text/javascript" src="./js/kenburned.min.js"></script>
    <script type="text/javascript" src="./js/mediaelement-and-player.min.js"></script>
    <!-- shortcode scripts -->
    <script type="text/javascript" src="./shortcodes/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.slicebox.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.eislideshow.js"></script>
    <script type="text/javascript" src="./shortcodes/js/camera.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/jquery.zaccordion.min.js"></script>
    <script type="text/javascript" src="./shortcodes/js/main.js"></script>
    <script type="text/javascript" src="./js/jquery.prettySocial.min.js"></script>
    <script type="text/javascript" src="./js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="./js/jquery.zoom.min.js"></script>
    <script type="text/javascript" src="./js/jquery.countdown.js"></script>
    <script type="text/javascript" src="./js/jquery.webticker.min.js"></script>
    <script type="text/javascript" src="./js/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="./js/selectize.min.js"></script>
    <!-- image filter -->
    <script type="text/javascript" src="./js/img-filter/jquery.gray.min.js"></script>
    <!-- // image filter -->
    <script type="text/javascript" src="./js/wow.min.js"></script>
    <script src="./syntax-highlighter/scripts/prettify.min.js"></script>
    <script type="text/javascript">
        $.SyntaxHighlighter.init();
    </script>
    <script type="text/javascript" src="./js/main.js"></script>
    <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8RV1M5dAKCJUP7USf_DSABjfCeNLX_ko&callback=googleMap"></script>
</body>

</html>
